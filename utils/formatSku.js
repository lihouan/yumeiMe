export function formatSku(that, res) {
    console.log('that,res: ', that, res);
    let goodsInfo = {
        _id: res.id, // 商品ID
        name: res.name, // 商品名称
        goods_thumb: res.mainPicture ?
            res.mainPicture :"https://images.merrige.cn/public/avatarc/avatar-1705640863602", // "http://images-test.merrige.cn/public/avatar-1697076670675", // 商品头像
        // 该商品对应的sku列表
        sku_list: res.items.length > 0 ?
            res.items.map((item, index) => {
                let sku_name_arr = [];
                item.specs.map((specsItem, i) => {
                    sku_name_arr.push(specsItem.v);
                });
                return {
                    _id: item.id,
                    goods_id: res.id,
                    goods_name: res.name,
                    image: res.mainPicture ?
                        res.mainPicture :"https://images.merrige.cn/public/avatarc/avatar-1705640863602", // "http://images-test.merrige.cn/public/avatar-1697076670675",
                    price: item.retailPrice * 100, //Math.floor(Math.random() * 24000),
                    // "price": item.retailPrice*100 ? item.retailPrice*100 : 0,
                    sku_name_arr: sku_name_arr,
                    // "stock": item.sellStockNum ? item.sellStockNum : 0,
                    stock: item.sellStockNum   //|| Math.round(Math.random() * 10),
                };
            }) : [{
                _id: res.id,
                goods_id: res.id,
                goods_name: res.name,
                image: res.mainPicture,
                price: res.retailPrice * 100,
                sku_name_arr: ["默认"],
                stock: res.sellStockNum || 0,
            }, ],
        // 商品规格列表
        spec_list: res.specList.length ?
            res.specList.map((item, index) => {
                let num = that.form.defaultSelect.num ?
                    that.form.defaultSelect.num : [];
                if (item.title == that.form.sizeName) {
                    that.sizeIndex = index;
                }
                return {
                    list: item.specValueList.map((x, i) => {
                        if (item.title == that.form.sizeName) {
                            return {
                                name: x.specValue,
                                selectNum: num.length > 0 ? num[i] : 0,
                                imgUrl: x.imgUrl || res.mainPicture,
                            };
                        } else {
                            return {
                                name: x.specValue,
                                imgUrl: x.imgUrl || res.mainPicture,
                            };
                        }
                    }),
                    name: item.title,
                };
            }) : [{
                list: [{
                    name: "默认",
                }, ],
                name: "默认",
            }, ],
    };
    let skuImageArr = res.specList[0].specValueList
    console.log('skuImageArr: ', skuImageArr);
    if (skuImageArr.length && goodsInfo.sku_list.length) {
        goodsInfo.sku_list.forEach(q => {
            skuImageArr.forEach(z => {
                if (q.sku_name_arr[0] == z.specValue) {
                    q.image = z.imgUrl
                }
            })
        })
    }


    return goodsInfo
}