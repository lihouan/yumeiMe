import http from './interface'

// 地址列表
export const addressList = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/address',
		method: 'get',
		params,
	})
}

// 新增地址
export const addressAdd = (data) => {
	return http.request({
		url: '/yumei-mall-customer/front/address',
		method: 'post',
		data
	})
}

// 查看地址详情
export const addressDetails = (id,params) => {
	return http.request({
		url: '/yumei-mall-customer/front/address/'+ id,
		method: 'get',
		params,
	})
}


// 更新地址
export const addressCompile = (id,data) => {
	return http.request({
		url: '/yumei-mall-customer/front/address/'+ id,
		method: 'put',
		data
	})
}

// 删除地址
export const addressDel = (id,data) => {
	return http.request({
		url: '/yumei-mall-customer/front/address/'+id,
		method: 'delete',
		data
	})
}

//设置默认地址
export const setIsDefault = (id,data) => {
	return http.request({
		url: '/yumei-mall-customer/front/address/'+id+'/default',
		method: 'post',
		data
	})
}



