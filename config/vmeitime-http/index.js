import http from './interface'
/**
 * 将业务所有接口统一起来便于维护
 * 如果项目很大可以将 url 独立成文件，接口分成不同的模块
 * 
 */
//设置请求结束后拦截器

// http.interceptor.request = (config) => {
// 	let i18nType;
// 	if (uni.getLocale() == 'zh-Hans') {
// 		i18nType = 'zh-CN'
// 	} else {
// 		i18nType = 'en'
// 	}
// 	// 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
// 	config.data = config.data || {}
// 	// 
// 	// 根据custom参数中配置的是否需要token，添加对应的请求头
// 	if (!config ?.custom ?.auth) {
// 		// 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
//         let token = store.state.loginInfo.loginToken.access_token || store.getters.loginToken
// 		config.header["yumei-auth"] =`Bearer ${token}`
// 	}
// 	config.header['x-tenant'] = store.getters.xTenant    //"mml" //mml 马来版
// 	config.header['accept-language'] = i18nType // 国际化配置

// }

export const baseUrl = () => {
	return http.getbaseUrl()
}


// 获取token
export const getloginToken = (data) => {
	return http.request({
		url: '/yumei-mall-auth/front/auth/token',
		method: 'post',
		data,
	})
}

//获取用户信息
export const getUserInfo = (params) => {
	return http.request({
		url: '/yumei-mall-auth/front/user/info',
		method: 'get',
		params,
	})
}

//注册
export const customerRegister = (data) => {
	return http.request({
		url: '/yumei-mall-auth/front/auth/token',
		method: 'post',
		data,
	})
}

//获取基础信息
export const settingGeneral = (params) => {
	return http.request({
		url: '/yumei-mall-common/front/setting/general',
		method: 'get',
		params,
	})
}

//查询支持手机区号列表
export const smsRegion = (params) => {
	return http.request({
		url: '/yumei-mall-message/front/sms/region',
		method: 'get',
		params,
	})
}

// 获取客服链接
export const customerServiceUrl = (data) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/customer-service-url',
		method: 'get',
		data
	})
}

// 验证码短信发送
export const smsCaptcha = (data) => {
	return http.request({
		url: '/yumei-mall-message/front/sms/captcha',
		method: 'post',
		data,
	})
}

// 重置密码
export const passwordReset = (data) => {
	return http.request({
		url: '/yumei-mall-auth/front/user/password-reset',
		method: 'post',
		data,
	})
}

//验证旧手机号是否验证码匹配
export const oldPhoneCheck = (data) => {
	return http.request({
		url: '/yumei-mall-auth/front/user/old-phone-check',
		method: 'post',
		data,
	})
}


//如果之前没绑定过手机号，或者旧手机号验证码通过后调用
export const changePhone = (data) => {
	return http.request({
		url: '/yumei-mall-auth/front/user/change/phone',
		method: 'put',
		data,
	})
}



//token校验是否失效接口
export const tokenAuthCheck = (data) => {
	return http.request({
		url: '/yumei-mall-auth/front/auth/check',
		method: 'post',
		data,
	})
}

//微信绑定
export const wechatBind = (data) => {
	return http.request({
		url: '/yumei-mall-auth/front/user/wechat-bind',
		method: 'post',
		data,
	})
}

// 邀请链接中携带token的注册链接调整为
export const registerByInvite = (data) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/register-by-invite',
		method: 'post',
		data,
	})
}

// 获取邀请链接参数
export const inviteParamsByToken = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/invite-params-by-token',
		method: 'get',
		params,
	})
}


// 获取邀请链接参数
export const applicationDetail = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/authorization/application',
		method: 'get',
		params,
	})
}

export const businessLevelAll = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/level/business',
		method: 'get',
		params,
	})
}

export const businessLevel = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/level/business-by-application',
		method: 'get',
		params,
	})
}


export const brandByInvite = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/by-invite',
		method: 'get',
		params,
	})
}

//经销商信息
export const customerInfo = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/info',
		method: 'get',
		params,
	})
}

//个人充值金额列表
export const summaryByAccount = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/bank-roll/summary-by-account',
		method: 'get',
		params,
	})
}

//获取e签宝实名认证地址
export const getSignAuthUrl = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/sign/auth',
		method: 'get',
		params,
	})
}

// 获取实名认证状态
export const flowStatus = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/sign/flow/status',
		method: 'get',
		params,
	})
}

// 订单数量小红点
export const purchaseOrderCount = (params) => {
	return http.request({
		url: '/yumei-mall-purchase/front/purchase-order/count',
		method: 'get',
		params,
	})
}

// 获取注册邀请链接参数
export const inviteParams = (data) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/invite-params',
		method: 'post',
		data,
	})
}

//获取e签宝实名认证地址(新)
export const getSignAuthUrlNew = (data) => {
	return http.request({
		url: '/yumei-mall-customer/front/sign/auth',
		method: 'post',
		data,
	})
}


// 身份证ocr
export const ocrIdCard = (data) => {
	return http.request({
		url: '/yumei-mall-common/front/ocr/id-card',
		method: 'post',
		data,
	})
}

//营业执照识别
export const ocrCreditCode = (data) => {
	return http.request({
		url: '/yumei-mall-common/front/ocr/credit-code',
		method: 'post',
		data
	})
}

//获取开户行列表
export const bankList = (data) => {
	return http.request({
		url: `/yumei-mall-finance/front/bank?bankName=${data.bankName}`,
		method: 'get'
	})
}

//大额行号查询
export const cnapsCode = (data) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/additional/cnaps-code',
		method: 'post',
		data
	})
}

//提交修改开户资料
export const additionalUpdate = (id, data) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/additional/update/' + id,
		method: 'post',
		data
	})
}


//证件类型
export const certificateTypeAll = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/certificate-type/all',
		method: 'get',
		params
	})
}

// 我的门店列表
export const getStoreApply = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/store-apply',
		method: 'get',
		params
	})
}

// 申请实体店记录
export const applyRecord = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/store-apply/apply-record',
		method: 'get',
		params
	})
}

//优化实体门店/生活馆详情
export const storeLiveDetails = (type) => {
	return http.request({
		url: `/yumei-mall-store/front/store-apply/by-type/${type}`,
		method: 'get'
	})
}

// 申请优秀实体门店条件判断
export const conditionCheck = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/store-apply/condition-check',
		method: 'get',
		params
	})
}



// 查询当前月份，是否有申请单
export const currentapply = (data) => {
	return http.request({
		url: '/yumei-mall-finance/front/customer/finance/currentapply',
		method: 'post',
		data
	})
}

// 商品组
export const productSearch = (params) => {
	return http.request({
		url: '/yumei-mall-search/front/product',
		method: 'get',
		params
	})
}

// 商品组2
export const productSearchPost = (data) => {
	return http.request({
		url: '/yumei-mall-pim/front/purchase-product/ids',
		method: 'post',
		data
	})
}

// 获取首页各模块数据
export const homeModule = (params) => {
	return http.request({
		url: '/yumei-mall-content/front/miro-page/home',
		method: 'get',
		params
	})
}


// 购物车列表
export const purchaseCartList = (params) => {
	return http.request({
		url: '/yumei-mall-cart/front/sale-cart',
		method: 'get',
		params
	})
}


// 购物车删除
export const purchaseCartDelete = (data) => {
	return http.request({
		url: '/yumei-mall-cart/front/sale-cart/item',
		method: 'delete',
		data
	})
}

// 购物车修改数量
export const purchaseCartAmend = (id, data) => {
	return http.request({
		url: '/yumei-mall-cart/front/sale-cart/item/' + id,
		method: 'post',
		data
	})
}

// 所有全选接口
export const purchaseCartSelectAll = (data) => {
	return http.request({
		url: '/yumei-mall-cart/front/sale-cart/item/select/all',
		method: 'post',
		data
	})
}

// 品牌全选接口
export const purchaseCartSelectAllBrandId = (data) => {
	return http.request({
		url: '/yumei-mall-cart/front/sale-cart/item/select-by-seller',
		method: 'post',
		data
	})
}

// 单选接口
export const purchaseCartSelectId = (id, data) => {
	return http.request({
		url: `/yumei-mall-cart/front/sale-cart/item/${id}/select`,
		method: 'post',
		data
	})
}

// 商品详情
export const productData = (id) => {
	return http.request({
		url: '/yumei-mall-pim/front/product/' + id,
		method: 'get'
	})
}

//加入购物车
export const purchaseCart = (data) => {
	return http.request({
		url: '/yumei-mall-cart/front/purchase-cart/item',
		method: 'post',
		data
	})
}


// 确认订单
export const purchaseOrderConfirm = (data) => {
	return http.request({
		url: `/yumei-mall-order/front/order/confirm`,
		method: 'post',
		data
	})
}

// 提交订单
export const purchaseOrderSubmit = (data) => {
	return http.request({
		url: `/yumei-mall-order/front/order/submit`,
		method: 'post',
		data
	})
}

// 品牌接口
export const brandAll = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/all',
		method: 'get',
		params
	})
}

// 商品列表
export const purchaseProductlist = (params) => {
	return http.request({
		url: '/yumei-mall-search/front/purchase-product',
		method: 'get',
		params
	})
}

// 购物车小红点
export const purchaseCartCount = (params) => {
	return http.request({
		url: '/yumei-mall-cart/front/purchase-cart/count',
		method: 'get',
		params
	})
}

// 商品素材
export const materialProduct = (params) => {
	return http.request({
		url: '/yumei-mall-post/front/material-product',
		method: 'get',
		params
	})
}



// 获取连连相关状态
export const additionalState = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/additional/state',
		method: 'get',
		params
	})
}

// 获取入驻状态
export const getCustomerAppliedTwoInfo = () => {
	return http.request({
		url: '/yumei-mall-customer/front/customer-apply-two-info/getCustomerAppliedTwoInfo',
		method: 'get'
	})
}








// 默认全部导出  import api from '@/common/vmeitime-http/'
export default {
	getCustomerAppliedTwoInfo,
	additionalState,
	materialProduct,
	purchaseCartCount,
	purchaseProductlist,
	brandAll,
	purchaseOrderSubmit,
	purchaseOrderConfirm,
	purchaseCart,
	purchaseCartList,
	purchaseCartDelete,
	purchaseCartAmend,
	purchaseCartSelectAll,
	purchaseCartSelectAllBrandId,
	purchaseCartSelectId,
	productData,
	homeModule,
	productSearch,
	productSearchPost,
	currentapply,
	getStoreApply,
	applyRecord,
	storeLiveDetails,
	conditionCheck,
	ocrIdCard,
	getSignAuthUrlNew,
	baseUrl,
	getloginToken,
	getUserInfo,
	customerRegister,
	settingGeneral,
	smsRegion,
	smsCaptcha,
	passwordReset,
	oldPhoneCheck,
	changePhone,
	tokenAuthCheck,
	wechatBind,
	registerByInvite,
	inviteParamsByToken,
	applicationDetail,
	businessLevelAll,
	businessLevel,
	brandByInvite,
	customerInfo,
	summaryByAccount,
	getSignAuthUrl,
	flowStatus,
	purchaseOrderCount,
	inviteParams,
	customerServiceUrl
}