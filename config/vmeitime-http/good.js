import http from './interface'

// 购物车列表
export const purchaseCartList = (params) => {
	return http.request({
		url: '/yumei-mall-cart/front/purchase-cart',
		method: 'get',
		params,
	})
}

// 购物车删除
export const purchaseCartDelete = (data,productType) => {
	return http.request({
		url: '/yumei-mall-cart/front/purchase-cart/item?productType=' + productType,
		method: 'delete',
		data
	})
}

// 购物车修改数量
export const purchaseCartAmend = (id,data) => {
	return http.request({
		url: '/yumei-mall-cart/front/purchase-cart/item/' + id,
		method: 'post',
		data,
	})
}

// 所有全选接口
export const purchaseCartSelectAll = (data) => {
	return http.request({
		url: '/yumei-mall-cart/front/purchase-cart/item/select/all',
		method: 'post',
		data,
	})
}

// 品牌全选接口
export const purchaseCartSelectAllBrandId = (data) => {
	return http.request({
		url: '/yumei-mall-cart/front/purchase-cart/item/select-by-brand',
		method: 'post',
		data,
	})
}

// 单选接口
export const purchaseCartSelectId = (id,data) => {
	return http.request({
		url: `/yumei-mall-cart/front/purchase-cart/item/${id}/select`,
		method: 'post',
		data,
	})
}

// 支付明细
export const skuAmountCalc = (data) => {
	return http.request({
		url: '/yumei-mall-store/front/bank-roll/sku-amount/calc',
		method: 'post',
		data,
	})
}