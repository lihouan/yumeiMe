import http from './interface'

// 保证金规则列表接口
export const cashDepositRuleList = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/cash-deposit-rule/list',
		method: 'get',
		params,
	})
}

//可划转金额资金显示明细接口
export const summaryByAccount = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/bank-roll/online/summary-by-account',
		method: 'get',
		params,
	})
}

//确认划转获取支付方式
export const specifyType = (params) => {
	return http.request({
		url: '/yumei-mall-pay/front/recharge/specify-type?payCode=lianlian_balance_pay',
		method: 'get',
		params,
	})
}

//验证银行卡快捷支付
export const payValidate = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/pay/validate',
		method: 'post',
		data,
	})
}

// 获取充值支付要素
export const rechargeParam = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/recharge/param',
		method: 'post',
		data,
	})
}